Nuestro proyecto consiste en adquisición de datos, procesamiento y representación gráfica de datos, monitoreo de valores entrantes, producción de productos textuales y gráficos.

Misión
Nuestro objetivo es implementar herramientas para distintos roles y flujos de trabajo, automátizando y generando productos e 
imágenes, y otras funcionalidades.

Visión
Crear interfaces de visualización de datos con confiabilidad, intuitivas y fáciles de usar. ¡Esperamos poder ayudarte!
